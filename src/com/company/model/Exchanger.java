package com.company.model;

public class Exchanger extends Organization{
    private int limitInUsd;

    public Exchanger(String name, float usd, int limitInUsd) {
        super(name, usd);
        this.limitInUsd = limitInUsd;
    }

    public int getLimitInUsd() {
        return limitInUsd;
    }

    @Override
    public float exchange(int uah) {
        float usd = super.exchange(uah);
        return usd < limitInUsd ? usd : 0f;
//        if (usd < limitInUsd) {
//            return usd;
//        } else {
//            return 0f;
//        }
    }
}
