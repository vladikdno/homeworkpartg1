package com.company.model;

public class Organization {
    protected String name;
    protected float rate;

    public Organization(String name, float usd) {
        this.name = name;
        this.rate = usd;
    }

    public float getRate() {
        return rate;
    }

    public String getName() {
        return name;
    }

    public float exchange(int uah) {
        return uah / rate;
    }

}
