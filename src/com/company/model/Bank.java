package com.company.model;

public class Bank extends Organization{

    private int limitInUah;

    public Bank(String name, float usd, int limit) {
        super(name, usd);
        this.limitInUah = limit;
    }

    public int getLimitInUah() {
        return limitInUah;
    }

    @Override
    public float exchange(int uah) {
        if (uah < limitInUah) {
            return super.exchange(uah);
        } else {
            return 0f;
        }
    }
}
