package com.company.model;

public class Generate {

    public static Bank getBank() {
        return new Bank("Oshad", 26.5f, 150_000);
    }

    public static BlackMarket getBlackMarket() {
        return new BlackMarket("BlackMarket", 26.3f);
    }

    public static Exchanger getExchanger() {
        return new Exchanger("Exchanger", 26.7f, 5_000);
    }



    public static Organization[] generateOrganization() {
        Organization[] organizations = new Organization[6];

        organizations[0] = new Bank("Приват", 26.3f, 150_000);
        organizations[1] = new Bank("Аваль", 26.4f, 150_000);
        organizations[2] = new Exchanger("Друг", 26.7f, 20_000);
        organizations[3] = new Exchanger("Благо", 26.8f, 20_000);
        organizations[4] = new BlackMarket("BlackMarket", 27.0f);
        organizations[5] = new BlackMarket("Second BlackMarket", 27.3f);

        return organizations;
    }
}
