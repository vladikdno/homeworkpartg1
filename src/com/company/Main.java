package com.company;

import com.company.model.Exchanger;
import com.company.model.Generate;
import com.company.model.Organization;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Organization[] organizations = Generate.generateOrganization();

        System.out.println("enter uah:");
        Scanner scanner = new Scanner(System.in);
        int uah = Integer.parseInt(scanner.nextLine());
//        System.out.println("Введите организацию");
//        String name = scanner.nextLine();
//
//        Bank bank = Generate.getBank();
//        BlackMarket blackMarket = Generate.getBlackMarket();
//        Exchanger exchanger = Generate.getExchanger();

//        for (Organization organization : organizations) {
//            organization.exchange(uah);
//            System.out.println(String.format("in %s: %.2f usd", organization.getName(), organization.exchange(uah)));
//        }

        for (Organization organization : organizations) {
            if (organization instanceof Exchanger) {
//                Exchanger ex = (Exchanger) organization;//cast(Exchanger) - Exchanger ex = organization; + Alt Enter
//                ex.getLimitInUsd();
                System.out.println(String.format("in %s: %.2f usd", organization.getName(), organization.exchange(uah)));
            }
        }

        Organization bestOrganization = organizations[0];//Для нахождения самого выгодного курса берем за основу первый массив
        //и называем его bestOrganization

        for (Organization organization : organizations) {//
            if (bestOrganization.getRate() < organization.getRate()) {
                bestOrganization = organization;
            }
        }

        System.out.println("Best organization is : " + bestOrganization.getName() + bestOrganization.getRate());

//        if (bank.exchange(uah) > 0f) {
//            System.out.println(String.format("in %s: %.2f usd", bank.getName(), bank.exchange(uah)));
//        }
//
//        if (blackMarket.exchange(uah) > 0f) {
//            System.out.println(String.format("in blackMarket: %.2f usd", blackMarket.exchange(uah)));
//        }
//
//        if (exchanger.exchange(uah) > 0f) {
//            System.out.println(String.format("in exchanger: %.2f usd", exchanger.exchange(uah)));
//        }
    }
}
